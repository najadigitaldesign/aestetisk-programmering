let mic;
let x = 350;
let y = 350;
let radius = 20; //remnants of an if-statement I tried to make with the buttons and mousePressed inside radius.

 function setup(){
  createCanvas(700, 700);
  background(110,160,160);
  textFont('Georgia');
  textSize(20);

    button = createButton('Yes');
      button.position(x-180, y-120);
      button.size(radius*2)
      button.mouseReleased(bounceSound);

    button = createButton('No');
      button.position(x+135, y-120);
      button.size(radius*2)
      button.mousePressed(lolbounceSound);

  mic = new p5.AudioIn(); // creates Audio Input
  mic.start(); // starts it
  startPage();

function startPage () {
  background(110,160,160);
    fill(230,220,180);
    text('will you let me use your audio so i can dance?',x-200,y-195)
    ellipse(x,y, 80, 80);
}

function bounceSound() {
  /* the circle is supposed to move up and down to the volume, but will only do so the moment the mouse is pressed.
  something should be added to do that, but I can't figure out exactly what*/

  background(110,160,160);
  // the circle is supposed to move up and down to the volume, but will only do so the moment the mouse is pressed. idk how to make it keep going.
  let vol = mic.getLevel(); // getLevel() reads the volume of an AudioIn
  let h = 350 - vol * height; //350 is just the start point

  fill(230,220,180);
  ellipse(x,h, 80, 80); // ellipse height is based on volume (let h from before)
  textFont('Georgia');
  textSize(34);
  text('make noise and look at me go!', 120, 550);
  }
}

function lolbounceSound() { //same thing for the "No"-button
  background(110,160,160);
  fill(230,220,180);
  textFont('Georgia');
  textSize(20);
  text('no worries, I already have it :)', 210, 550);

  let vol = mic.getLevel();
  let h = 350 - vol * height;
  fill(230,220,180)
  ellipse(x,h, 80, 80);
}
