[RUNME](https://najadigitaldesign.gitlab.io/aestetisk-programmering/miniX4/) <br>
[source code](https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX4/miniX4.js)

<img src="minix4start.png" alt="miniX4start" width="400"/> 
<img src="miniX4.gif" alt="næ" width="400"/> 
<img src="miniX4no.png" alt="miniX4no" width="400"/> 
<br>
<br>

**Title: Dancing Data** <br>
The circle is supposed to be an unreliable source that can take data whenever it pleases (just ignore that your browser asks you to agree in the beginning, hehe), and is supposed to symbolize nonconsentual capture of data. <br>
It is supposed to not care about whether you agree or reject the proposal, and will move up and down to the volume of the audio coming from your mic. <br>

I present to you, the age of data capture where people's data are a ~~commodity~~ treasure and is being captured even if they don't know it. It is not always a poorly read terms of service, because it can hide behind vague descriptions from the company, where the everyday person doesn't know what could entail from agreeing to "will you let the program tailor the experience to you?" - did you ever explicitely say yes to Facebook knowing what you write, even if you never send it? Dancing Data ~~spies on you~~ listens and dances in celebration to your precious ~~captured and saved~~ voice! Don't worry about agreeing to long proposals, we already capture you the moment we meet!

**Troubles and learning moments** <br>
The circle was supposed to "jump" according to the volume of mic-input after pressing the button, but it only uses the volume the moment the button is pressed (I am talking while clicking the button on the gif). I know that it is because of the button.mousePressed(function-that-records-the-volume-and-makes-the-circle-move); in my code, but I couldn't find any alternatives that I was able to make work. <br>
I've found that the capturing of volume stops working after some time on the url too, so there's that.. maybe you can only really see it in action on the GIF here.
I've later learnt that other people have trouble with p5.sound being unreliable, but the solutions seem to advanced to me. <br>
All in all I did learn how incorporate mic capturing, even if it doesn't always work.

**Data capture** <br>
I was inspired by this line "In the internet of things, the device serves you, and you serve the
device. Indeed we become “devices” that generate value for others." (Soon & Cox 2020) In my program, if you press "Yes", you exchange your data for some entertainment; a ball dancing/jumping to the sound of your voice. The user may not think about whether or not their voice is being saved as well, and may unknowingly exhange data with an online source. I could have taken another path than fear-mongering, but that was what I had time for that week. After reading about Facebook keycapturing, there must be many other sites who do the same, where the "exchange of goods", so to say, is an underlying thing that only one party really knows the full scope of, because when we use free sites, it is not always so "free"; you pay with your data, your identity, preferences to be an easier costumer to other companies to advertise to whether it be materialistic or idealistic. This is further expands to the users perhaps relying a lot on the data the programs mirror, as it is seen on multiple tracking apps; maps, diet tracker, period trackers, moodtrackers etc. Here it is assumptively (is that a word?) an aware exchange, as the user can see their own data being shown to them in order, but it is a very interesting topic, and if I were to continue the paranoid vibes of my program, I _could_ speculate on how that data is being used for other things than just the users health or what they are tracking... but I'm not going to :-)

I had to cut this miniX4 very short due to my hand/arm being achy from overuse (musearm), so this is all I could manage to do with the time I had monday and a bit of tuesday :)

**Ideas!** <br>
To reflect on the readings though, it would have been really cool to code something that reflected on the point made in Datafication about how our devices "tell us who we are, what we are feeling, and what we should be doing (...)" (Mejia & Couldry, 2019). I'm thinking a program that will ask a series of things and then determine your personality type/type in partner/ideal sleep pattern or whatever aspect of a person I could have chosen for the miniX. It would be cool to make a comment about how much software can play a role in identity and personality today (I'm looking at you, Co-Star (jokes)).
My current program is focused more on paranoia and a "gotcha"-vibe.

**New Syntaxes (to me)** <br>
createButton() <br>
<img src="https://cdn.discordapp.com/attachments/325430399289393174/953285415467630623/unknown.png" alt="getLevel();" width="80"/> 


Describe your program and what you have used and learnt.
Articulate how your program and thinking address the theme of “capture all.”
What are the cultural implications of data capture?

Signed, strained hands.

----
References: <br>
Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119 <br>
Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. <br>
https://p5js.org/examples/sound-mic-input.html
