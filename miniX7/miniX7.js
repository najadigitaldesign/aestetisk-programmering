let bun = [];
let x = 200; //200 pixels from the center is where the rabbits will spawn
let y = 200; // -,,-
let approach;
let flee;
let keysmash = 0;

function preload() {
  rabbit = loadImage('jumpingRabbit.gif');
  player = loadImage('player.gif');
  uhoh = loadImage('uhoh.gif');
  saved = loadImage('saved.png');


  frameRate(70);
}

function setup() {
 createCanvas(windowWidth, windowHeight);
 angleMode(DEGREES);
 imageMode(CENTER);
 noStroke();
/* (remnants of me trying to incorporate mic input. it worked! it just messed with a game mechanic)
 mic = new p5.AudioIn();
 mic.start();
 getAudioContext().resume()
*/
}

function draw() {
  /* let vol = mic.getLevel(); (mic colume levels)
  let maxvol = 0.5; */

  background(220,200,130);
  translate(width/2, height/2);

//  let flee = map( vol,0, 1, height, 0); (the rabbits that would run away from the player when yelling)
  let approach = (x-0.2, y-0.2);

if (approach>0) {
  x = x-0.2;
  y = y-0.2;
}

if (approach>69) { // if the rabbits are father away that 69 pixels, normal player sprite.
  image(player, -5, 5,80,60);
}

if (approach<70) { // if less than 70 pixels, change the player sprite to be more nervous.
  image(uhoh, -5, 5,80,60);
}
/*
if (vol>maxvol) { (if the volume is more than maxvol, flee from player)
  x = x-0.2;
  y = flee;
}
*/

if (keysmash > 77) { // WIN. keysmash just detects how many times keys have been pressed. you need to press 77 times to win.
  textStyle(ITALIC);
  textFont('Georgia');
  fill(120,140,220);
  textSize(20)
  image(saved, -5, 5,80,60); //still image of the sprite instead of a .gif
  text("thank youuuu ☀",-68,-100)
  noLoop();
}

if (keysmash < 1) {
  textStyle(ITALIC);
  textFont('Georgia');
  fill(120,140,220);
  textSize(23)
  text("QUICK, PRESS ANY KEY",-120,-330)
}

if (keysmash < 30 && keysmash > 0) {
  textStyle(ITALIC);
  textFont('Georgia');
  fill(120,140,220);
  textSize(23)
  text("PRESS REPEATEDLY UNTIL THEY STOP!",-215,-330)
}

if (keysmash > 29 && keysmash <90) {
  textStyle(BOLDITALIC);
  textFont('Georgia');
  fill(120,140,220);
  textSize(23)
  text("GO, GO, GO !!",-80,-330)
}

if (approach<0) { // LOSE if the rabbits reach 0 (the middle)
  noLoop();
  textStyle(ITALIC);
  textFont('Georgia');
  fill(0);
  textSize(18)
  text("an unfortunate death.",-85,-100)
}

  for (let i = 0; i < 15; i++) { // The 15 rabbits
    rotate(24);
    bun[i] = new Bun (x,y);
    bun[i].showBun();
  }
}

function keyPressed() {
keysmash++ // it plusses the 'keysmash' ie. it adds to how many times the key has been typed.
}

class Bun {
  constructor(bunx, buny, bunsize) {
    this.xsize = bunsize
    this.xpos = bunx
    this.ypos = buny
}
    showBun() {
      image(rabbit, this.xpos, this.ypos, this.size);
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
