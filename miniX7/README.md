[RunMe](https://najadigitaldesign.gitlab.io/aestetisk-programmering/miniX7/) <br>
[source code](https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX7/miniX7.js) <br>

<img src="win.gif" alt="win" width="400"/> 
<img src="lose.gif" alt="lose" width="400"/> <br>
<br>


**Which MiniX have you reworked - what have you changed and why?** <br> 

I reworked my miniX6, where we had to work with object abstraction/object orientation and make a game.
I changed my approach going into the mechanics of the game. The purpose of the original was to write the sentence “please sleep” a number of times until the rabbits stopped approaching the player. However, I didn’t get any further than changing the colour of the letters “pl” and couldn’t figure out how to use keycodes and keyinput. Now the player just has to press any button a number of times until the rabbits stop. I made this change because it was within my scope of learning and made the game possible. <br>
I added a player that was not there before and added indicators as to what to do with the text “PRESS ANY BUTTON” and “REPEATEDLY PRESS UNTIL THEY STOP”. The player can refresh the page to make a new best, but I have not included a “press SPACE to restart” for example, as I wanted the death to stand out. It makes the game a little creepier in my opinion, and the death in this game space seems to hold just a little more weight that way. <br>

<img src="player.gif" alt="player" width="60"/>

**What have you learnt in this mini X? How did you incorporate/advance/interprete the concepts in your ReadMe/RunMe (the relation to the assigned readings)?** <br>

With the new details, there is a sense of urgency provided by the approaching objects (rabbits) and the goal the player has to reach before the death of the blob in the middle. In my miniX6 the player is not given a goal and the objects do not seem as dangerous, since there is nothing on the screen to protect. The objects’ role change in this new version, making their approach seem more urgent. The object is essentially reframed.
Another way to look at it could be that now the player has some hope. In the old game (miniX6) there was nothing the player could do to win, and there wasn’t any indication that they had lost either. The player now has an objective. Something that remained the same, was that the player can’t move the sprite, they can only try to make the danger stop approaching.
I want to refer to this quote: “Computation not only abstracts from the world in order to model and represent it; through such abstractions, it also partakes in it.” (Soon & Cox, 2020)
What is really going on behind the scenes, is the program counting how many times the keys have been pressed out of 77, but the player doesn’t know that. The counting of keypresses and countdown for the approach (the counting of the pixels) is hidden from the player, and that is also an abstraction, as they don’t see exactly what is going on. They only know that the text has told them. By abstracting away from that aspect of the code, it creates uncertainty the same way

> Moreover, in order to discuss the aesthetic dimensions of code and computational processes, we incorporate artistic works that explore the material conditions of software and the operations of computational processes as practical and theoretical examples.” (Soon & Cox, 2020)

My game is not critical on the outside, but through the process of coding it, I began to experience the ways that object orientation has an impact on the game space, and how my own decisions – even though the game is simple – of what to include/exclude could still make big changes in the game. Here I’m talking about things such as; why a rabbit? What does the context do to this otherwise harmless rabbit? Then I remembered of how scared I was of the rabbit in Monty Python when I was little, and how my game could uphold that scary image of a rabbit suddenly being carnivorous. In the last 7 weeks I have been able to train my critical thinking in regards to code and computational culture while creating a framework (the programs) of which to discuss the topics. I’ve been able to use my own programs to discuss topics as time in digital culture, emojis and politics, the discussed literacy of coding among other things, and have in turn also contributed to those discussions with my own code (though the reach is limited of course, only my classmates have seen it, but we have all contributed to the cultural discussions with our programs.) Most of the weeks I would say that I have succeeded in creating critical-aesthetic objects (Soon & Cox, 2020) - the objects being my miniXs - and even though they needed a ReadMe in order to be fully formed criticisms/discussions, they were still a materialization of something I'd read/chosen to discuss. <br>

<img src="saved.png" alt="heart" width="70"/> <br>
----
References <br>
Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164 <br>
Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 14-19
