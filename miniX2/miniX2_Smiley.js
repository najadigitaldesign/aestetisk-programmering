/* I start out choosing what I want the different variables to be by writing "let".
I have made x,y and x2,y2 because I want to refer back to them later! Instead of writing ellipse(200,200) later in draw, it'll just be "ellipse(x,y)" Both of these things I figured out from https://p5js.org/examples/input-easing.html*/
let x = 200;
let y = 200;
let x2 = 450;
let y2 = 200;
// easing means how far behind the circle will be behind [the mouse].
let easing = 0.09;
/* Instead of defining the ellipse dimensions like ellipse(88,88) I've defined the radius instead!
This will let the code find the middle of my emojis, when it has to detect if my mouse is clicking on them or not! */
let emojiRadius = 44;

/* Ah, simple stuff. Just the background and where I want my text and so on :) */
function setup() {
  createCanvas(720, 400);
  noStroke();
  textSize(30)
  textAlign(30,30);
}
/* OKAY, so normally when we just write "fill" it will fill **every shape/text/object** with that colour.
I wanted the eyes and text to be a different colour than the emoji-body. To do that, I just choose a name - example: presstext = color(r,g,b). (I chose presstext because my text says "press the emojis!". You can name it anything!)
I do that because then I can be specific about WHICH colour i want to put on WHAT. I found this on the second example here https://p5js.org/reference/#/p5/color */
function draw() {
  background(227, 224, 93);
  // Here's the WHAT-part. I put my presstext(rgb) above what I want to colour. In this case; text('press the emojis!',220,60);
  let presstext = color(230,154,0)
  fill(presstext);
  text('press the emojis!',220,60);

// THE TWO EMOJIS
// See! Now I just named the colour "emojis" ! and then I say "fill the colour of the emojis into the two ellipses underneath"
  emojis = color(139,166,48);
  fill(emojis);
  ellipse(x, y, emojiRadius * 2);
  ellipse(x2, y2, emojiRadius * 2);

//EYES
  eyecolor = color(255,255,165);
  fill(eyecolor);
  //EYES on LEFT EMOJI
  ellipse(x+20,y-10, 7, 10);
  ellipse(x-20,y-10, 7, 10);
  //EYES on RIGHT EMOJI
  // The reason it says x2 and y2 is because if I just used "x" and "y" again from lines 3+4, there would just be two emojis on the same location. On line 5+6 I made the x2,y2.
  ellipse(x2+20,y2-10, 7, 10);
  ellipse(x2-20,y2-10, 7, 10);

//WHEN MOUSE PRESSED (This is the rest of the code. From here on you'll only se code for when the mouse is pressed.)
      //LEFT EMOJI
  // Here, my "if" statement says; if the distance between my mouse and the x-y coordiantes (the ones in line 3+4) is less than the radius (that means if the mouse is inside the emoji) and (&&) the mouse is pressed, then begin the things underneath. This one line I took from 11:45 on The Coding Train https://youtu.be/TaN5At5RWH8?t=705
  if (dist(mouseX, mouseY, x, y) <= emojiRadius && mouseIsPressed) {
    emojiPressed = color(139,166,48);
    fill(emojiPressed);
    /*The reason I make a new ellipse (body of emoji) is because on line 80+82 I make some new ellipses(eyes) follow the mouse!
    So by adding a new ellipse emojibody I overwrite the previous eyes. I just put a big green circle on top of them, just like when we delete shapes by putting a background on top of them :))*/
  ellipse(x, y, emojiRadius * 2);
  /*This chunk of code is taken from https://p5js.org/examples/input-easing.html and makes the emoji follow the mouse in a slow manner.
  I haven't been able to find out what *exactly* targetX means, but I THINK it means that the target of the movement will now be the mouseX (and later mouseY)*/
    let targetX = mouseX;
    // the x-movement of the emoji will follow targetX(the mouseX) minus x. That means it will follow the mouse but it will be x pixels behind the mouse.
    let dx = targetX - x;
    /*+= means that the value of the right is being added to the value on the left of the +=. So that now the emoji's x will be easing when pressed.
    It says that the x = the movement * the easing (defined as 0,09 on line 8). We define what x is.*/
    x += dx * easing;

// Here we do the same for the y-coordinates.
    let targetY = mouseY;
    let dy = targetY - y;
    y += dy * easing;
    let b = color(80);

/*Aaaaaand HERE! The new set of eyes I talked about on line 54
  So, I give these new eyes the same colour as before, easy, we've seen this before.*/
      //EYES WHEN PRESSED
    movingeyes = color(255,255,165);
    fill(movingeyes);
  /* THEN I replace the eyes' x,y-coordinates with the x,y-coordinates of my mouse! The eyes will now follow my mouse.
  Though, I want to adjust where they are placed (you can try and run it with only mouseX and mouseY to see the start-position), so I say "follow mouseX but shift the x-coordinate 20 pixels to the left" (if had written mouseX + 20 it would have been shifted 20 pixels to the right)
  Then I tell mouseY to go 10 pixels down. The result is now that the left eye on the left emoji follows the mouse AND has the same spacing as before I pressed the mouse. SOURCE for the eye-placement when mouse is moved: https://editor.p5js.org/TeeReggler/sketches/ByCenwJsb */
    ellipse(mouseX - 20, mouseY - 10, 7, 10);
  // Same thing, just for the right eye.
    ellipse(mouseX + 20, mouseY - 10, 7, 10);

      //RIGHT EMOJI
// NOW for the other emoji! My "else if" says "if I press the emoji (y'know within its radius as we tried before) with the x2,y2-coordinates instead, then the things underneath will happen:
  } else if (dist(mouseX, mouseY, x2, y2) <= emojiRadius && mouseIsPressed) {
// Makin' eyecolours again.
    eyecolor = color(139,166,48);
    fill(eyecolor);
/*Here, I set the x2,y2 of the emoji coordinates to be set at random, but that it should only jump 5 pixels at a time on x, and jump 20 on y.
If I had set the value to be random(-1,1); the tremble would be very small. The shakyness SOURCE https://p5js.org/examples/interaction-tickle.html */
    x2 += random(-5,5);
    y2 += random(-20, 20);
    //RIGHT emoji's pressedEYES - just the normal eyes re-inserted from earlier
    ellipse(x2 - 20, y2 - 10, 7, 10);
    ellipse(x2 + 10, y2 - 5, 7, 10);
/* So what happened here was, that if the mouse reaches the inside the emoji, the emojis x,y-coordinates will be random (it'll shake and move!) */
  }
}

// NAJA HAMMERSHØJ ANICKA (sorry for the mess)
