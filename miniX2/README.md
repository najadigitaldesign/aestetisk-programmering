<h3>EMOJIS!</h1>
<img src="https://stickercommunity.com/uploads/main/29-09-2020-21-10-03-abc5.webp" width="50" height="50" /> <br>
<img src="miniX2.gif" alt="miniX2" width=""/> 

**What I Made** <br>
I made two interactable emojis! They look the same though (reason further down). When you hold down the left mousebutton on each of them, they react differently.
One is curious and "likes" the interaction, while the other one is quite anxious and finds it very uncomfortable.

🟢 **The Geometric Shapes** 🟢 <br>
This time we had to experiment further with geometric shapes, but since I hadn't used any in my miniX1 (I used .png files) I had to get familiar with new syntaxes. I had started out making a curvedVertex, because I wanted a soft looking ellipse (like a piece of dough on a counter or something), but that was a too time consuming for me, so I ended up with the circles.
I wanted to make the emojis different from each other and for them to make a new face when pressed, but I ran out of time! So I ended up having them look the same, but they kind of *are* two different emojis, because they have different personalities. In the beginning I had tried made them pastel green/pastel orange, but with the TEXT in mind, they looked kind of white, so I just made them a darker shade of green so that they remain circles without turning too human. I did this with Femke Snelting in mind; humanoid emojis can generate unnecessary race problematics so that maybe expanding the human emojis isn't the best approach. Rather, having non-human smileys could potentially solve the identity side of emojis and representation (is my intrepretation.)
I could also have made triangles or other fun shapes and be more artsy (which I would have liked a lot too!), but I got really hooked on the idea of having an anxious emoji and a curious one that had different movements. I've been quite busy lately, so I couldn't put as much time into this as I wanted to aesthetically, unfortunately.

😊 **Politics/societal context** 😖 <br>
With this littles mess of a code, I wanted to touch upon that what might be okay for some (the left emoji😊) might be uncomfortable for others (right emoji😖) and that we have to be mindful of people's boundaries :)) I got inspired by someone who made their emoji do a thing, but I now realize that I may have diverted a bit too much away from the prompt, which was aesthetics-focused, whereas my emojis are more action-focused 😥 oops.
It would be fun if these emojis existed in everyday life and all emojis looked the same, until you touch them and they move in a certain way; shaky could be nervous, rapid movement could be excitement/anger, swirly movements could be silly and so on. We would have to think a lot about the meanings and there would probably be (sub)cultural differences to what the movements mean, just like emojis today sometimes have different meanings depending on the crowd (I remember seeing a chair-emoji in 2021 being used as a laughing emoji for a couple of months on the internet). The movements would perhaps be more abstract than the emotional faces from normal emojis in terms of figuring out the emotion. It would be interesting if you could choose your own geometric shape too! No faces: Just symbols and shapes that moved on the screen in different patterns. Of course, there can always be made subvalues for symbols, so that for example racists would use a certain moving pattern to signal that to each other, but I think it would be a new and exciting way to communicate emotions online.

💭 **Thoughts about my Code** 👩‍💻 <br>
I experimented A LOT with the radius part of my code, and especially had to think (and search) hard about how to make the eyes of the left emoji stay somewhat inside the emoji AND follow the mouse.
I definitely would not use this code in an exam, as I haven't figured out what targetX is *precisely* - I have an idea of what it does, but I can't really use "I have an idea" in an exam situation.
I was introduced to a lot of new syntaxes this time, and I have had many tries and errors with them, and even though I succeeded, in the future I would like to watch more The Coding Train-videos and learn more simple ways to do the things that I did - I am convinced it was not the most straight-forward way, as I mixed the new lines of code I learned  with the ones I already knew, instead of using the whole Dan-setup. Example of this: I used one line of code from a The Coding Train-video and used it in my own code, which did not have the same kind of setup/syntaxes/stuff as Daniel Shiffman, and I just made some workarounds to make it work, because I didn't have time to learn all the new syntaxes that was in video 7.4.
I am sure that there are simpler ways to write the code, but I do not have those skills yet and tried to make do with what I could understand. I was just really excited about the movement and wanted to try and implement it! 🌟

miniX2 URL: https://najadigitaldesign.gitlab.io/aestetisk-programmering/miniX2/ <br>
code: https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX2/miniX2_Smiley.js <br>
miniX2 GIF: https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX2/miniX2.gif <br>

Naja Hammershøj Anicka 🌻

-----
References: <br>
Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70 <br>
Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016 [Video, 1 hr 15 mins]. <br>
Daniel Shiffman, Code! Programming with p5.js, The Coding Train [online] Available at: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. [watch 1.3,1.4,2.1,2.2]
