                                                  // NAJA HAMMERSHØJ ANICKA ✿
                                  //  ----------------------------------------------------
//I'm preloading the images so that the code has them ready, when I want to display them. From p5.js references; image().
function preload() {
  img = loadImage('uden_bagg.png');
  jordbær = loadImage('jordbær_miniX1.png');

}
/*Setting up the canvas and background colour, then inserting the preloaded image with the desired y+x+h+w coordinates.
From p5.js references; image()*/
function setup() {
  createCanvas(800,800);

}
/*The reason it says "img" inside the paranthesies is because that is what we called my png file in line 5!
img = whatever image-file I put in there. Next time it's going to be (jordbær, x , y, height, width)*/
function draw(){
  background(180,233,233)
  image(img, 330,300,90,90);
  image(jordbær, 150,160,70,90);
  /*My goal is to feed the bunny a strawberry. To do that, I've made an "if-then" statement, where if the mouse is pressed, then a new strawberry-image will have the same placement as the mouse' x and y-coordinates; it follows the mouse. I just replaced the x and y-coordinates with "mouseX" and "mouseY" to do this.
The reason why I have added the background again, is so that when I press the mouse, the strawberry disappears from the start-location. I hide it with the background. I also make sure to re-insert the bunny-file, so that it doesn't disappear as well.
If I had not added the background again, a strawberry-image would be following my mouse WHILE there would still be a strawberry at the startposition*/
    if (mouseIsPressed) {
      background(180,233,233)
      image(img, 330,300,90,90);
      image(jordbær,mouseX,mouseY,70,90)

  /*Now, this is where it goes wrong. I wanted to code, so that the strawberry would disappear when reaching the bunny's coordinates.
  All the code over this comment makes it so that you can pick the strawberry up, and when you release the mouse it jumps back to its start-position.
  I have turned the code under this comment into a comment as well, so that you can still run the javacript (the code underneath ruins it) while being able to see my attempts at making my goal in line 28 work!
}
      MY ATTEMPT AT MAKING THE STRAWBERRY DISAPPEAR (by telling it only to display the bunny and background) IF IT REACHES THE BUNNY'S COORDINATES:
    else if (mouseX == 330) && (mouseY == 300) {
      background(180,233,233)
      image(img, 330,300,90,90);

      Note: To my understanding the "if" means "if mouse is pressed, then make the strawberry follow mouse" and "else if" is like an addition "if the mouse then reaches the coordinates (while being pressed), then make the strawberry disappear."
            Something is clearly wrong though, so if you have any suggestions, feel free to write them!
            Console/Undersøg says the "&&" in line 33 is at fault, but I haven't been able to find any alternatives (not any that work at least).

    } HERE I TRY TO SAY "IF THE NONE OF THE ABOVE HAS HAPPENED, STAY IN PLACE. "IF this happens; do this. ELSE; stay the same""
    else {
      background(180,233,233)
      image(jordbær, 150,160,70,90)
      image(img, 330,300,90,90);
*/
  }
}
