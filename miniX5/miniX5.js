let inc = 0.004; // an increment variable. this will determine how much the x-value will be (how frantically the rectangle will move. it looks desperate if the value is any higher than 0,01. I made it 0,004, but making it 0,01 however looks really quick and exciting!)
let start = 0; // since the offset of x will always start at 0, and I want the x offset to be different everytime, I give it the name "start" (will be returning to this further down.)

function preload() {
  catbop = loadImage ('cat-jam.gif');
}


function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(80);
}

function windowResized() { //the canvas size follows the window size even if it is changed.
  resizeCanvas(windowWidth, windowHeight);
}
function draw() {
  background(170,160,190);
  fill(250,100,160)
  strokeWeight(1)
  stroke(200,240,240);

let m = millis();
  if (m < 40000) {
  let xoff = start; // xoff stands for x offset. noise operates between 0 and 1-- here we use start! so now it will not always start at 0, because in the last line of code I've tied it to the increment, which makes it so that the offset also changes by the increment :)
  let xoff1 = start; // same thing. explanation for another xoff further down.

   for (let x = 0; x < windowWidth/2; x++) { // here I tell the program, that if the amount of rectangles is less than the number of pixels of the windowWidth, then make more rectangles. I don't need to tie it to the windowWidth, I could easily have written (let x = 0; x < 1000; x++) instead for example, but I wanted the number of rectangles to adjust to the window size :)) The "tail" of the wave disappears because there should only be as many rectangles as the windowWidth/2, BUT it still has to keep creating the new ones, so I has the delete the ones in the back. (This i am still in doubt about, but this is what I have concluded myself.)
     let y = noise(xoff) * windowHeight;
     let x = noise(xoff1) * windowWidth; // if I'd only used xoff for both of them, the rectangle would only have moved in a straight, tilted line from upper left to bottom right. By adding xoff1 I make sure that it can also move the other way and therefore move all over the canvas.

     xoff += 0.001; // i'm making this value way smaller so that the rectangles behind the one in front don't float too far away, if that makes sense? you can copy/paste my code into your atom and see what I mean.
     xoff1 += 0.01; // the value isn't too high here for the same reason, but I could allow it to be higher than the first one without the pattern looking too wild.

     rect (x,y,100,200)

     xoff += inc; //
}

} else { // this is my for-fun-code :b
  let xoff = start;

   for (let x = 0; x < windowWidth-300; x++*x++) {
     let y = noise(xoff) * windowHeight; // I didn't add the same things as above, as I only wanted this one to be fixed and horizontal.

     image (catbop, x,(y),100,100)

     xoff += inc;

  }
}
//triangle(x,y-50,width,200,80)

start += inc; // tying the "start" to the values of the increment.
}

// Naja Hammershøj Anicka - have a nice day ^^
