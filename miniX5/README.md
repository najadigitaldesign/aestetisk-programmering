[RUNME](https://najadigitaldesign.gitlab.io/aestetisk-programmering/miniX5) <br>
[source code](https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX5/miniX5.js/)

<img src="rectWave.gif" alt="rectWave" width=""/> <br>
(screen recorder had a hard time recording. the RUNME is nicer)

**Rectangle waves!**
---
The rules are:
-	After 4000 milliseconds have gone by, change to easter egg (can be viewed further down, if you don’t wanna wait in the RunMe.)

First part:
-	If x is less than the number of pixels of the window’s width, make more rectangles.
-	If the window is adjusted, the “wave” gets smaller (because of less rectangles as stated above)
-	The rule about the x-offset for the noise syntax tells the wave not just to start at one, but have an offset somewhere else. The offset will be different each time the page is loaded.

Second part:
-	Keep making (easter egg) if x is less than windowWidth-300

The role of the rules is to keep the moving patterns clean and within the limits of the window while looking random. They make sure that things don’t look too messy, and go by as smoothly as I could manage to make it!
It ties to the theme of Pseudo Random Number Generation, and how it isn’t entirely random where the rectangle appears and where it goes next – it never goes off-screen or rapidly over to the edge of the width. This is because of the noise() syntax, because noise always (kind of) stays around the middle of things. Another purpose I used noise for was smoothness. The direction of the rectangle isn’t radically changed, but keeps to values that are close to each other, only diverting once in a while. This also makes sure that the same value is not repeated many times in a row, because then the rectangle would be “standing still” (generated many times in the same spot) instead of being dynamic.

The rule, that there is only as many rectangles as half the window width, has an important role, because even though this would have been an ongoing generative “art piece” if there were 4000 rectangles for example, the current disappearance of the “tail” makes it very temporary and new all the time. All these generated rectangles makes it look like an entity moving around, and not just a bunch of generated shapes, and I thought that looked cool! If I add the 4000+ rectangles it kind of looks like a sea, which is really cool as well, but I chose the current amount. <br>

🌊 <br>

In general the continuous generation of the rectangles makes it look wavey and without the shape generation I could not have made the same effect with my current skills (I am not familiar with 3D for example, but the wave looks a little 3D thanks to regeneration.) It is interesting to look at which direction the noise syntax chooses to go, and the “randomness” and rules makes it more worthwhile to look at.
Likewise, I really like the cat-half of my project, but its rules are more shortlived, since it only moves up and down, making a trail to the left (it looks the best with the edge of the .gif that way.) I would say the rules (movement and windowsizing) still makes it interesting to look at though.

<img src="bopcat.gif" alt="bopCat" width=""/> 
<br>
<br>
I started out with the 1D graph from Coding Train’s 1D Perlin Noise video and then tinkered with it to make it look like the final visuals (both the rectangle- and catwave). I mixed the graph with the principles of making a circle move randomly on the screen and to my surprise it worked! <br>
The Coding Train graph:

<img src="1D_Graph_Shiffman.gif" alt="1dGraph" width="400,300"/> <br>
<br>
I want to say (write) that I could definitely have used the conditional statement in a more interesting way (instead of making an if-statement regarding milliseconds), as changing the actions of the rectangle-wave would have been more interesting to the eye than changing to a different visual (the cat). Then the generative project would look like it evolved instead of just changing. <br>
<br>

**Thoughts about generative art and who the 'author' is** <br>
In the process of coding this project, I found myself getting surprised over and over again, because something unexpected happened. While I don't consider the machine, the computer to be a co-artist, I feel like it is a collaboration between the ones behind p5.js and me. They made this piece of code possible and provided libraries along with a nifty coding language. It is true that none of this could be seen, if there was no machine to process the lines, but I think that what I described before feels more right.
I also completely disaggree with the opinion that artists using this method are not 'real artist' and coding/generative art is not 'real art'. It seems incredibly elitist, and doesn't really live up to the creative image the art world would like to have. Instead, I think it should be embracing a different form of utilizing artistic skill and explore the possibilities - for one, coding is way cheaper than buying a lot of art supplies, if you have a laptop/PC, and may open up the art world to a lot more people.

---
References:

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 <br>
Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146. <br>
The Coding Train - I.4: Graphing 1D Perlin Noise
https://www.youtube.com/watch?v=y7sgcFhk6ZM&ab_channel=TheCodingTrain
