function setup() {
  create canvas 700,700
  background(300,300,300)
}

function drawGrid() {
 cols = width/grid_space;
 rows = height/grid_space;
 let arr = new Array(cols);
 for (let i = 0; i < cols; i++) { //no of cols
   arr[i] = new Array(rows); //2D array
   for (let j = 0; j < rows; j++){ //no of rows
     let x = i * grid_space; //actual x coordinate
     let y = j * grid_space; //actual y coordinate
     stroke(0);
     strokeWeight(1);
     noFill();
     rect(x, y, grid_space, grid_space);
     arr[i][j] = 0;  // assign each cell with the off state + color
   }
 }
 return arr; //a function with a return value of cell's status
}
