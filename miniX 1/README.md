<img src="miniX1.gif" alt="miniX1" width=""/> 

**🍓 What have you produced?**

I have produced a small sketch, where you can move a strawberry around by leftclicking and holding in the mouse. It will then jump back to its start-position when releasing mouse1 (the left-click button). The goal was to be able to feed the bunny the strawberry; all attempts were futile. It is a 800x800 square in bright cyan-ish colour, displaying only a .png of a bunny and a .png of a strawberry. I planned on having some text displayed as well, but got caught up in trying to make the feeding-part work. I have produced a small little start to something interactable! It is not meant to be a pet-simulator, but it was meant to be a "drag these thing to the bunny, and it will become happier and happier" kind of thing. It could also have been a human, but I think the bunny is cute.


👩‍💻 **How would you describe your first independent coding experience (in relation to thinking,  reading, copying, modifying, writing code, and so on)?**

I've learned a lot by watching other peoples' code and then seeing an example next to it, so I can follow what's going on. I haven't used much of the code I found online in my miniX1, since I could only /almost/ use them for what I was trying to do, BUT I think I learned a lot regardless, because I now have an easier time navigating the layout of a couple of different p5.js codes. I can understand why a line of code is placed where it is for example, which I had a harder time understanding just a couple of days ago!
I was surprised that I, as time went on that day, could freestyle a few lines of code, because I had become more familiar with the p5.js structure. Of course Atom helped me by suggesting some syntaxes, when I started writing a word to see if it existed in the p5.js language.
It is also really nice to see how others have done it, as I had made some unclean and messy code to get the strawberry to follow the mouse (I had tried some bits and pieces from the p5.js references), but then I watched a Coding Train video about getting a circle to follow the mouse, which showed a very short amount of code, so I used that line of code instead for my .png-file!
Troubleshooting was fun as well, even though my code wasn't working, because I became really hooked on trying to solve the problem by myself (and with the help of console once or twice haha).


**✍️ How is the coding process different from, or similar to, reading and writing text?**

I find that coding is a lot similar to trying to tell a story - trying to make something happen - though with a lot more editing involved every- or every third line.
When I write, new content only adds to the existing written words, but when I code, one line of code can make everything disappear! It is quite fun getting used to the white screen in the testing tab. I guess it could be similar to a story with plot holes - if new chapters don't correlate with the rest of the story and the reader is left behind.
Writing and coding are both very creative outlets, and can make any visual image we want - on screen or inside our minds - and it is interesting looking into the history of literacy. There are quite a few similarities in the arguments as to why citizen should be literate in written languages, and the more modern arguments that pushes coding/programming to be an added literacy to the citizens repertoire. The similarities I'm thinking about here are the ways that people have argued that these two things are good skills to have to engage in-, criticize- and understand society better.


**💭 What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**

For a long time I only saw it as a skill to be utilized - to produce games, websites, systems etc. Later I thought more about the ethics, focusing on who makes the code (the diversity) and why there should be more different kinds of people involved. However I had not thought about how a coding language can have a social impact (one example would be the master-slave naming) and how not just ideas (what elements a website is going to consist of) but the coding languages and code can impact that as well. That the things written behind the scenes have great value and impact! In short, I used to just think of the _results_ of the code and who worked on the code and didn't know to think of coding itself! It ties back to how code and written words are similar, as words can signal various values (morally/politically/socially, not coding-values like (50,90)).


repository URL: https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/tree/main <br>
code URL: https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX%201/sketch.js <br>
URL that runs the code: https://najadigitaldesign.gitlab.io/aestetisk-programmering/Test/ <br>

References
----
Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
The Coding Train: https://www.youtube.com/watch?v=7A5tKW9HGoM&ab_channel=TheCodingTrain <br>
WillMclean (I watched it muted): https://www.youtube.com/watch?v=OMrstvdA0iI&ab_channel=WillMclean <br>
Both images are placeholders and are from a twitter account:  <br>
bunny - https://twitter.com/sylviahan213/status/1398902878815744001?s=20&t=1kv0GafqD-c27SklFyw03Q <br>
strawberry - https://twitter.com/sylviahan213/status/1376098477390716937?s=20&t=1kv0GafqD-c27SklFyw03Q <br>
