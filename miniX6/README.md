[RunMe](https://najadigitaldesign.gitlab.io/aestetisk-programmering/miniX6/)<br>
[source code](https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX6/miniX6.js) <br>
<img src="miniX6.gif" alt="miniX6" width=""/> 


**In the following, I will be writing my ReadMe as if I succeeded with my program, because I had a lot of thoughts about object abstraction in regards to it, even though I couldn’t manage to finish my code.**

My "game" has two objects; the rabbits and the player (supposed to be in the middle). The player is helpless in this world, and cannot move to escape, and so has to type their way “out”. In this game space, rabbits are a threat, and the player can’t move, contrary to what we can do in the real world – I assume not many symmetrical rabbit ambushes take place in the real world. In this space the player has to repeatedly type the words “please sleep” a number of times, which in the end will make the rabbits stop approaching and sleep instead, which is also vastly different from our real world interactions with rabbits. (I can feel the feedbackers going “... well of course” haha.)
In my “game” I have left out interactive movement, and chosen that this world will instead react upon receiving words instead of jumps/attacks or movement at all. <br>
**The idea was,** that if the player typed “please sleep” a certain amount of times, the rabbits would stop approaching and sleep instead. The letters were supposed to be one colour and would change if the key was pressed, so that the player could see that their input had been registered. That’s kind of where I got stuck and only made the first "p".

I have abstracted far away from what we would normally experience around a group of rabbits. This might perhaps seem somewhat realistic for people with leporiphobia (the fear of rabbits, apparently!), where it visualizes the fear, but for the majority the rules of this world are very game-like: you just kind of accept that “oh, these are dangerous, I better do what the screen says” and it makes sense for the player. The reality I have created (or aspired to create, really) makes me think of other games, where presented concepts are just accepted, because of a certain expectation to the game; an example could be a thought such as “oh, the wizard is slender, because of course it is.” The example I just gave has also become such a stable that many probably expect it, and are not just accepting a new concept, but that these worlds have chosen how a wizard looks and it follows the mold. That was a tangent, as my own project doesn’t follow a very big mold – at least I can’t think of any tropes.. actually, except for Monty Python now that I think about it.

Had I made the dangerous objects into something related to an issue, for example the amount of additional responsibilities capitalism can throw upon people (work/life balance, commercial pressure to buy products under the guise of selfcare, the pressure to set and reach #goals at an exponential pace, pressure to capitalize your hobbies etc.), the real life uneasiness of these things are abstracted into something that will end the game and the player, if the player can’t make them “sleep” or meet the ideal “do it all”-kind of person. Here, the abstraction makes for a visual metaphor, where it can be seen how overwhelming it can feel for humans to try and live up to the ever spinning and approaching expectations (objects). Even though I can’t relate a lot, I can definitely see the trend, and that this kind of approach would make for some good discussion too with a variety of topics.

Going back to the rabbits, they could also symbolize the character’s worries or troubles overall, so that these animals, that are often associated with cute things, all of a sudden feel eery in this game space.


… SO, as you can see, what I described here can’t be seen in my program, as I couldn’t execute it this time around, but now all my intentions and reflections are written down.

Have a nice daaaaay, <br>
Naja. ☼

Edit 3rd of April: <br>
When I think about the code that *is* there, I think there is something to be said. This game is a game in a very loose sense. There is no objective really, and the player is confused. I have made a second version here: <br>
Runme 2.0 (there was supposed to be a new link, but Atom is troublesome atm) <br>
I just finished the sentence, because even though I couldn't execute my original idea, I think that I could have written something interesting, had I added the whole sentence. **Well, I was about to do all this**, but Atom decided to clear all my folders and after re-adding them it can't start the server so I can see what I code... neat. SO this is going to be an imaginary I guess. Having the whole sentence "please sleep" while is blinks red every time a key has been pressed makes for a very eery atmosphere and by leaving out all the game mechanics I've added in miniX7, this game world is suddenly quite hopeless - the player doesn't know why the rabbits should sleep, or how to make them sleep. The message could also be interpreted as saying that the player should sleep. A very ominous vibe is created and the player doesn't know from the get-go that there is no more to the game. <br>
I want to throw this quote in here: "(...) how a computer actually works are largely hidden from view and/or substituted by desktop metaphors (e.g. deleting a file by throwing it in the “bin”)." (Soon & Cox, 2020) I like that this imagined version as well as the original one plays with the expectations of abstraction. I think that the player would think that there is something hidden. That there is something to be accomplished (maybe only if they go in expecting it to be a game instead of a weird visual) and that they have to do something to get to the next happening, because they would think that that information is just hidden when it was never there. There is nothing more to the game.

---
References: <br>
Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164 <br>
Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).
