![sleep](miniX3/markdownSleepbun.gif)
<br>
Sundial time and rabbits <br>
[RUNME](https://najadigitaldesign.gitlab.io/aestetisk-programmering/miniX3/) <br>
[source code](https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX3/miniX3.js)
<br>
Note: You have to change time zone on your device to see the different throbbers if you don't want to wait. Chrome doesn't like manual time changes. <br>
<img src="markdownday.gif" alt="awake gif" width="500"/> 
<img src="markdownday.png" alt="awake img" width="435"/> <br>

**🐇 What I want to explore and/or express 💤 <br>**
My program is split into two different throbbers; one for daylight and one for sundown. If the time is before 18:00, a lighter throbber (above) will play, and if it is later than that, a darker throbber will play (scroll down).

I want to try and defy linear time, "where the amount of light is just a variable external to steady beat of time." (Lammerant, 2018), because I think that today it can be tied into capitalist society where time is economic and work hours can be extended, because the day doesn't end at sundown like it did many many years ago. <br>
I want to try and connect the user more to sundial time, by making it seem like the working rabbits get tired at night (maybe urging the user to slow down as well), and that the page takes time loading because it is night time and the rabbit is worn out. Meanwhile at daytime it is loading because the rabbits are trying hard to make it work. I want to explore the idea of bringing more awareness to our natural cycles, and how this could look through a throbber. ~~This is ironic, since I am coding and writing this way past sundown.~~ <br>

I can already reveal that the daytime "busy rabbit"-part did not work. I could not figure out how to make the rabbits disappear and reappear in the middle (having a limit to how far they could run until reappearing at the start), so instead they just run off the page. Now, instead of a "we are setting up the page for you!"-vibe, the rabbits have an unreliable and "seems it went wrong! .. well!"-vibe. So unfortunately the two different throbbers are not coherent in the same way the original idea for them was, but I think that the daytime-one still plays with the expectations of a throbber and its message to the user, and is not very traditional, which I like. <br>

**🔁 Syntaxes & one function! 🕗 <br>**
I used the hour(); syntax, since it was a simple way to showcase my idea! Of course, this idea will not work as well in the summer time, since the hours in my code are set to the daylight we have now in early March. I used it to have a connection to Hans Lammerant's writings about sundial, which had some point about how we have remodelled the present and when we are "out of time" - because it is certainly not at sundown anymore.
I wanted to use time-related syntaxes to change between two images in the daytime throbber (sitting rabbit + running rabbit) to make an animation, but I couldn't figure out how to put them in a loop. I am sure that there is a simple way to loop them - I just haven't found it yet! Instead I opted for a homemade .gif with the two images and preloaded it in my code.

I used the for(loop); to make the number of rabbits adjust to how big the window size is and situated them in the middle (before they run off.) <br>

translate(); was used to translate the starting point of the rabbit to be centered. example: translate(width/2,height/2);. In my code it is width/2.5 because I wanted the line of rabbits to be more to the left. <br>

function windowResized  I found in the throbber example from Winnie Soon (references) and it resizes the canvas to the window size, even if changed.

Time is a *web* of different processes in computational history (at least to me). Very simply, most computers today run on Network Time Protocol (NTP). This protocol syncronizes devices to Coordinated Universal Time (UTC) which is a linear time measurement.
P5.js's hour() syntax communicates with system's NTP and applies it to the program - in this case, in my if(); else(); statement, where if the clock is before 18PM and after 8AM, the daytime sequence plays. If the time is anything else, play the nighttime sequence. During the screenrecordings of both throbbers, I discovered that Chrome refuses to run my program, if my device is not syncronized with UTC! I had set the time manually but had to go back and change time zone instead.

**What a throbber typically communicates/hides** 🔄 <br>
Typical associations with throbber: waiting, working, "oops!", "we are setting up the page for you". Throbbers communicate that something is being set up and it is working to get something ready for you, when in reality the program "failed" to load the screen in time. Sometimes it is not a failure, but simply something implemented by the makers for the user to get the impression that the program works hard for them. What those throbbers do, is reassure the user that the program is trying to fix up a page for them, or fix a problem, so the user should wait. I myself used the "oops! you weren't supposed to see this" at daytime but my throbber does not indicate that it is trying to find the right page for you. It simply does not take responsibility and flees. I *could* have given it a service attitude, if I have changed the text to "let us go get the page for you", because then they would run off to try and fix things. Both my throbbers have an "oh well" feel to them, which I think is different to a lot of other throbbers I have encountered. While the first one is irresponsible, the nighttime one is just tired and says there is nothing, telling the user to just rest.<br>
<br>
🌙☀️ **Additional thoughts** 💭 <br>
My concept will of course feel unfair to those who live in regions with perpetual daylight, since they would overwork themselves if sun was the key factor to productivity, and vice versa for people with little daylight. My project is based on very local sunlight.
<br>

![sleep](miniX3/markdownnight.gif)
<br>
References <br>
Hans Lammerant, The Techno-Galactic Guide to Software Observation (2018), pp. 88-98 <br>
Soon & Cox, Aesthetic Programming: A Handbook of Software Studies (2020), pp. 80+90 <br>
Throbber from Winnie Soon https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class03#33-sample-code
