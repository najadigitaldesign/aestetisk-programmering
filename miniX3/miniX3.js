let running = 0;

function preload() {
  rabbitSleep = loadImage('sleepingRabbit.gif');
  rabbitJump = loadImage('jumpingRabbit.gif')
}

function setup() {
  createCanvas (windowWidth, windowHeight);
  frameRate(30);
}

function draw() {
    background (200,150,200,150);
    translate(width/2.5, height/2);
    drawBunnies();
}

function drawBunnies() {
let h = hour();     //p5.js has a function that follows the hour of your system! hour();
  if (h < 18 && h > 8) {    // if the time is before 18:00 ond after 08:00
    textSize(20);
    textFont('Sans Serif');
    fill(230,230,150)
    text("oops! it is not loading",10,-110);
    text ("bye!",80,135);

      for(let x = 0; x < width/4; x += 100){ // this for(Loop); is made so that the wider the window is, the more rabbits there are (I didn't want that many so I just divided the width until I was satisfied)
      image(rabbitJump,x - 85 + running,-30,170,120); // the reason I write "x - 85" is because I want to rabbits to appear in the middle. 0 is in the middle, and the first rabbit appears in the middle, but the rest spawn to the right of it, so I just moved the start-rabbit a little bit to the left.
  running = running + 1.5; // ^ the "+ running" means that the rabbit's x-position will be at -85 plus the value I assign to running. In this line I say that running is 1.5 pixels. SO The rabbits will move 1.5 pixels to the right.
  }
}  else {   // if the time is after 18:00 and before 08:00
  background (10,50,100);
  fill (230,230,230);
  image(rabbitSleep,0,-20)
  fill(180,170,240);
  textSize(14);
    text("no page",80,0);
  textSize(20);
    text("just rest",70,130);
    }
}

function windowResized() { //the canvas size follows the window size even if it is changed.
  resizeCanvas(windowWidth, windowHeight);
}

// NAJA HAMMERSHØJ ANICKA (have a nice day~)
